<?php

namespace App\Traits;

use Illuminate\Support\Facades\Http;

trait NeoApiTraits
{
    public function fetchneodata($startdate, $enddate)
    {
        $response = Http::get('https://api.nasa.gov/neo/rest/v1/feed', [
            'start_date' => $startdate,
            'end_date' => $enddate,
            'api_key' => config('services.neokey')
        ]);
        return $response->json() ;
    }
}
