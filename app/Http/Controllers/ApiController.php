<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\NeoApiTraits;

class ApiController extends Controller
{
    use NeoApiTraits;
    /**
     * Fetch data from neo api.
     *
     * @param  Request  $request
     * @return void
     */
    public function __invoke(Request $request)
    {
        //validate the user input
        $validatedData = $request->validate([
        'startdate' => 'required|date',
        'enddate' => 'required|date',
        ]);
        //use trait method
        $result=$this->fetchneodata($validatedData['startdate'], $validatedData['enddate']);
        $fast_astroid=0;
        $closest_astroid=0;
        foreach ($result['near_earth_objects'] as $value) {
            foreach ($value as $v_value) {
                $v_value=collect($v_value)->recursive();

                //calculate fastest astroid
                $v=$v_value->get('close_approach_data')[0]->get('relative_velocity')->get('kilometers_per_hour');
                if ($v>$fast_astroid) {
                    $fast_astroid=$v;
                    $fast_astroid_id=$v_value->get('id');
                }

                //calculate closest astroid
                $dv=$v_value->get('close_approach_data')[0]->get('miss_distance')->get('kilometers');
                if ($closest_astroid==0) {
                    $closest_astroid=$dv;
                    $closest_astroid_id=$v_value->get('id');
                }
                if ($dv<$closest_astroid) {
                    $closest_astroid=$dv;
                    $closest_astroid_id=$v_value->get('id');
                }
            }
        }

        //chart values prepration
        $chart = collect($result['near_earth_objects'])->mapWithKeys(function ($item, $key) {
            return [$key=>count($item)];
        });

        //Response Prepration
        $result=[
            'fast_astroid'=>[
                'speed'=>$fast_astroid,
                'id'=>$fast_astroid_id
            ],
            'closest_astroid'=>[
                'speed'=>$closest_astroid,
                'id'=>$closest_astroid_id
            ],
            'chart'=>$chart

        ];
        return $result;
    }
}
